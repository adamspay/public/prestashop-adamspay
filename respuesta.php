<?php

require_once dirname(dirname(dirname(__FILE__))) . '/config/config.inc.php';
require_once dirname(dirname(dirname(__FILE__))) . '/init.php';
require_once dirname(__FILE__) . '/adamspay.php';

class respuesta
{

    public function __construct()
    {

    }

    function respuestaVoid()
    {


        /**
         * Escuchamos la respuesta de AdamsPay
         */

        // Ejemplo respuesta AP
        // __URL__&intent=pay-debt&app=prestashop&type=debt&doc_id=mitiendaon_10
        // Traemos el doc_id

        $url_actual = parse_url($_SERVER["REQUEST_URI"]);


        // [Migs] Algunos controles para sanitizar un request externo
        //
        $intent = !empty($_GET['intent']) && is_string($_GET['intent']) ? $_GET['intent'] : null;

        if ($intent === 'pay-debt') {
            $doc_id_adamspay = !empty($_GET['doc_id']) && is_string($_GET['doc_id']) ? $_GET['doc_id'] : null;
            $partes = explode('_', $doc_id_adamspay);
            $id_orden = count($partes) > 1 ? $partes[1] : null;
        } else {
            $id_orden = null;
        }

        // @done: eliminado control de url origen como pendiente.
        if ($intent == 'pay-debt' && $id_orden) {


            // Leemos el estado de la orden del hook webhook.php
            // $leer = new adamspay();
            // $leer->modificarEstadoOrdenPagado($id_orden[1]);

            $url_redireccion = __PS_BASE_URI__ . "index.php?controller=order-detail&id_order=" . $id_orden;

            Tools::redirect($url_redireccion);


        } else {

            // No es intento de pago (No hay ID de orden)
            Tools::redirect(__PS_BASE_URI__ . "index.php");

        }

    }

}

$r = new respuesta();
$r->respuestaVoid();