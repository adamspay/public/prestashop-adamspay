# Módulo PrestaShop - AdamsPay.com (Versión 0.1)
### Adamspay - Cobros online para tu eCommerce

+ Monedas habilitadas:
    - Guaraní (PGY)
    - Dólar Americano (USD)
    - Euro (EUR)

+ Configuraciones:
    - api_key: El Api Key de tu Aplicación
        - Configuración / Aplicaciones / Congigurar
    - Modo producción: El módulo se habilita por defecto en PRUEBAS.
    Una vez configurado el api_key se debe pasar a producción.
=
### Documentación para configurar la Aplicación:

https://wiki.adamspay.com/devzone:adm:adm-ui