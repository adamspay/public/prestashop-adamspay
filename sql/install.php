<?php

$sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'adamspay_pagos` (
  `id_orden` int(11) NOT NULL AUTO_INCREMENT,
  `url_pago` varchar(255) NOT NULL,
  PRIMARY KEY (`id_orden`),
  UNIQUE KEY `id_orden` (`id_orden`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$creoTablasCorrectamente = false;

	if (Db::getInstance()->execute($sql) == false) {
        // Si hay algún error al crear tablas, entones
        $creoTablasCorrectamente  = false;
	}
	else{
        $creoTablasCorrectamente = true;
    }
