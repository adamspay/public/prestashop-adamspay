<?php

class AdamspayValidationModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $cart = $this->context->cart;

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'adamspay') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->trans('This payment method is not available.', array(), 'Modules.Checkpayment.Shop'));
        }

        $customer = new Customer($cart->id_customer);

        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);

        // Validamos la orden y obtenemos el $id_order
        $this->module->validateOrder((int)$cart->id, Configuration::get('PS_OS_CHEQUE'), $total, $this->module->displayName, null, null, (int)$currency->id, false, $customer->secure_key);
        $id_order_modificar = Order::getIdByCartId((int)$cart->id);

        // _PS_BASE_URL_.__PS_BASE_URI__."modules/adamspay/";


        // Preparamos el POST a AdamsPay
        $prefijo_tienda = Configuration::get('ADAMSPAY_PREFIJO_WEB');

        $customer = new Customer();

        // Si es invitado, espera a ver si pagó, sino devuelve al carrito.
        if($customer->is_guest){

        }
        // Si está logueado, genera la orden y envía a AdamsPay
        else{
            $this->hacerPostAdamsPayVoid($this->formatMoneda($total), $id_order_modificar, $prefijo_tienda, $currency->iso_code);
        }

    }


    // @todo [Migs]
    // Presentar mensaje correcto al usuario cuando se genera un ERROR

    function hacerPostAdamsPayVoid($montoPagarParam, $idPedidoParam, $prefijo_tienda, $param_iso_moneda)
    {

        $apiUrl = $this->obtenerApiUrl() . '/debts?update_if_exists=1';
        $apiKey = Configuration::get('ADAMSPAY_API_KEY_APP');

        /*
         * El ID Deuda se compone por ADAMSPAY_PREFIJO_WEB + el ID de Orden confirmada de prestashop.
         * En cada orden se guarda el ID  de deuda, entonces, si cambia el parametro ADAMSPAY_PREFIJO_WEB
         * cambiaría el formato ID Deuda pero nosotros buscaríamos de lo que se guardó en cada orden.
         * */
        $idDeudaParaPost = $prefijo_tienda . $idPedidoParam;

        // Hora DEBE ser en UTC!
        $ahora = new DateTimeImmutable('now', new DateTimeZone('UTC'));
        $expira = $ahora->add(new DateInterval('P2D'));

        // Crear simple deuda (Caratula o Label configurable)

        $deuda = [
            'docId' => $idDeudaParaPost,
            'label' => Configuration::get('ADAMSPAY_LABEL_DEUDAS'),
            'amount' => ['currency' => $param_iso_moneda, 'value' => $montoPagarParam],
            'validPeriod' => [
                'start' => $ahora->format(DateTime::ATOM),
                'end' => $expira->format(DateTime::ATOM)
            ]
        ];

        // Crear JSON para el post
        $post = json_encode(['debt' => $deuda]);

        // Hacer el POST
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $apiUrl,
            CURLOPT_HTTPHEADER => ['apikey: ' . $apiKey, 'Content-Type: application/json'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post
        ]);

        $response = curl_exec($curl);
        if ($response) {
            $data = json_decode($response, true);

            // Deuda es retornada en la propiedad "debt"
            $payUrl = isset($data['debt']) ? $data['debt']['payUrl'] : null;
            if ($payUrl) {

                // Si creó la deuda grabamos el ID DEUDA en la ORDEN ya inmutable.
                // @TODO: GRABAR ID DEUDA
                Tools::redirect($payUrl);

            } else {
                // @TODO: Mostrar lindo mensaje sino se puede crear deuda
                $mensaje = "No se pudo crear la deuda: ";
                Tools::redirect('index.php?controller=order&step=1&mensaje='.$mensaje);
                //print_r($data['meta']);
            }

        } else {
            echo 'curl_error: ', curl_error($curl);
        }
        curl_close($curl);
    }

    function obtenerApiUrl()
    {

        $apiUrl = null;

        if (Configuration::get('ADAMSPAY_MODO_PRODUCCION') == 1) {
            $apiUrl = Configuration::get('ADAMSPAY_URL_PRODUCCION');
        } else {
            $apiUrl = Configuration::get('ADAMSPAY_URL_PRUEBAS');

        }

        return $apiUrl;

    }

    /**
     * Formatea los decimales, segun codigo ISO de moneda.
     * @todo: se puede obtener directo los decimales configurados.
     * */
    function formatMoneda($montoFormatear)
    {
        $montoFormateado = null;
        $moneda = $this->context->currency;
        if ($moneda->iso_code == 'PYG') {

            $montoFormateado = number_format($montoFormatear, 0, '', '');

        } elseif ($moneda->iso_code == 'USD') {
            $montoFormateado = number_format($montoFormatear, 2);
        } elseif ($moneda->iso_code == 'EUR') {
            $montoFormateado = number_format($montoFormatear, 2);
        }

        return $montoFormateado;
    }
}
