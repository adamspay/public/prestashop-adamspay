<div class="panel">
    <div class="row adamspay-header">

        <div class="col-xs-12 col-md-4">
            <h3>Ayuda con la configuración:</h3>
            <ul>
            <h5>

                <li>Modo Producción</li>
                Indica si el módulo está en modo pruebas o modo producción.
                En la <a href="https://staging.adamspay.com/merchants/" target="_blank">UI de Comercios <span
                            class="icon icon-external-link"></span></a> debe indicar si el sistema está en pruebas
                producción de
            </h5>
            <h5>
                <li> AdamsPay Api Key</li>
                Debe conseguir el Api Key de su aplicación en UI de Comercios / Configuración / Aplicaciones / Api Key
            </h5>
            <h5>
                <li>Url Pruebas</li>
                URL que se enviará los pedidos cuando el sistema está en modo pruebas
            </h5>
            <h5>
                <li>Url Producción</li>
                URL que se enviará los pedidos cuando el sistema está en producción
            </h5>
            </ul>


            <h4>
                <li>Ayuda adicional:</li>
                Si necesita ayuda adiconal en la configuración del Módulo, puede
                <a href="mailto:info@adamspay.com">enviarnos un email </a> a info@adamspay.com.
            </h4>
            </ul>
        </div>
    </div>

    <hr/>

    <div class="adamspay-content">
        <div class="row">
            <div class="col-md-6">

            </div>

            <div class="col-md-6">

            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-md-12">


                <div class="row">


                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
