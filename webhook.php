<?php
require_once dirname(dirname(dirname(__FILE__))) . '/config/config.inc.php';
require_once dirname(dirname(dirname(__FILE__))) . '/init.php';
require_once dirname(__FILE__) . '/adamspay.php';

/*
 * Validación AdamsPay.com
 * Más info: https://wiki.adamspay.com/devzone:concepts:webhook#validacion
 * */

$json_ap_post = file_get_contents('php://input');
$datos_json = json_decode($json_ap_post);

// Calculamos el md5 de la concatenación de: "adams"+POST+Api_Secret
$hmac_calculado = md5('adams' . $json_ap_post . Configuration::get('ADAMSPAY_API_SECRET_APP'));
$hmac_rx = @$_SERVER['HTTP_X_ADAMS_NOTIFY_HASH'];

if ($hmac_calculado !== $hmac_rx) {
    die('Validación ha fallado'); // Ignorar esta notificación
} else {

    // Todo OK: Procesar

    // Obtener solo el Status
        if (isset($datos_json->notify) &&
            isset($datos_json->notify->type) &&
            $datos_json->notify->type === 'debtStatus')
        {
            $idOrden = $datos_json->debt->docId;
            $idOrden = explode('_', $idOrden);
            $estadoOrden = $datos_json->debt->payStatus->status;
            $payUrl = $datos_json->debt->payUrl;

            if ($estadoOrden == 'paid') {

                $leer = new adamspay();
                $leer->modificarEstadoOrdenPagado($idOrden[1], $payUrl);

            } else {
                //PS_OS_ERROR @todo: Ver si modificamos el estado del pedido a error en pago

            }
        } else {
            // ignorar
            echo 'ignored';
        }

}