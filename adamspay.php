<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class adamspay extends PaymentModule
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'adamspay';
        $this->tab = 'payments_gateways';
        $this->version = '0.5.0';
        $this->author = 'Adamspay.com';
        // Cambiar este parametro si necesitamos mostrar una advertencia a futuro en la página de modulos
        // Ejemplo: Que este modulo requiere ser configurado.
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Adamspay - Cobros online para tu eCommerce');
        $this->description = $this->l('Adamspay es un sistema de cobro online para aplicaciones móviles y sitios web que permite recibir pagos a través de todos los canales habilitados por el comercio.

Está compuesto por un servicio en la nube y el API con el cual se enlaza al software del comercio.');

        $this->limited_countries = array('PY');

        $this->limited_currencies = array('PGY', 'USD', 'EUR');

        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_);
    }

    public static function modificarEstadoOrdenPagado($idOrdenParam, $payUrl)
    {

        // @TODO: Validamos que la orden no esté cancelada o anulada etc.
        // Si pago debt->payStatus->status == "paid", cambiamos el estado a pagado

        $order = new Order($idOrdenParam);
        $history = new OrderHistory();
        $history->id_order = (int)$order->id;
        // Ponemos el estado de la orden a Pago remoto aceptado.
        $history->changeIdOrderState(Configuration::get('PS_OS_WS_PAYMENT'), (int)($idOrdenParam));

        // Agregamos info Adicional Link de Pago en Adamspay
        $db = \Db::getInstance();
        $db->insert('adamspay_pagos', array(
            'id_orden' => (int) $idOrdenParam,
            'url_pago' => pSQL($payUrl),
        ));

    }

    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('La extension cURL es requerida para instalar este módulo');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false) {
            $this->_errors[] = $this->l('No disponible en tu Pais');
            return false;
        }

        $url_pruebas = "https://staging.adamspay.com/api/v1";
        $url_producion = "https://checkout.adamspay.com/api/v1";

        // Agregamos en la DB la config de ADAMSPAY
        // Solo al instalar usa la funcion. Puede cambiar dps.
        Configuration::updateValue('ADAMSPAY_PREFIJO_WEB', self::acotarNombreTienda());
        // La descripción, caratula o Label para la deuda en Adamspay
        Configuration::updateValue('ADAMSPAY_LABEL_DEUDAS', Configuration::get('PS_SHOP_NAME'));
        Configuration::updateValue('ADAMSPAY_API_KEY_APP', '');
        Configuration::updateValue('ADAMSPAY_API_SECRET_APP', '');
        Configuration::updateValue('ADAMSPAY_MODO_PRODUCCION', false);
        Configuration::updateValue('ADAMSPAY_URL_PRUEBAS', $url_pruebas);
        Configuration::updateValue('ADAMSPAY_URL_PRODUCCION', $url_producion);


        //@TODO: Ver solo los Hook utilizados, no registrar innecesariamente otros.
        return parent::install() &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('actionPaymentConfirmation') &&
            $this->registerHook('actionOrderStatusUpdate') &&
            $this->registerHook('displayPayment') &&
            $this->registerHook('displayPaymentReturn') &&
            $this->registerHook('displayAdminOrderLeft') &&
            $this->registerHook('displayOrderDetail') &&
            $this->registerHook('displayPaymentTop') ||
            $this->installDb();

    }

    /**
     * @hook displayAdminOrderLeft
     */
    public function hookDisplayAdminOrderLeft()
    {
        $id_order = Tools::getValue('id_order');

        $link_pagos = Db::getInstance()->getValue('
			SELECT `url_pago`
			FROM `'._DB_PREFIX_.'adamspay_pagos`
			WHERE `id_orden` =  '.(int)$id_order
        );

        $this->context->smarty->assign(array(
            'link_pagos'=> $link_pagos
        ));

        return $this->display(__FILE__, 'views/templates/admin/admin_ordenes.tpl');

    }

    /**
     * @hook displayOrderDetail
     */
    public function hookDisplayOrderDetail()
    {

        return "hookDisplayOrderDetail";
    }


    public function installDb() {
        require_once dirname(__FILE__) . '/sql/install.php';
    }

    public function uninstall()
    {

        Configuration::deleteByName('ADAMSPAY_PREFIJO_WEB');
        Configuration::deleteByName('ADAMSPAY_LABEL_DEUDAS');
        Configuration::deleteByName('ADAMSPAY_API_KEY_APP');
        Configuration::deleteByName('ADAMSPAY_API_SECRET_APP');
        Configuration::deleteByName('ADAMSPAY_MODO_PRODUCCION');
        Configuration::deleteByName('ADAMSPAY_URL_PRUEBAS');
        Configuration::deleteByName('ADAMSPAY_URL_PRODUCCION');
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitadamspayModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('produccion', Configuration::get('ADAMSPAY_MODO_PRODUCCION'));
        $this->context->smarty->assign('api_es_null', (Configuration::get('ADAMSPAY_API_KEY_APP') == '') ? true:false);
        $this->context->smarty->assign('secret_es_null', (Configuration::get('ADAMSPAY_API_SECRET_APP') == '') ? true:false);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configuracion.tpl');

        return $output . $this->renderizarFormulario();
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {

        $prefijoControlarValidar = Tools::getValue('ADAMSPAY_PREFIJO_WEB');
        $prefijoControlarValidar =  self::acotarNombreTienda($prefijoControlarValidar);


        Configuration::updateValue('ADAMSPAY_PREFIJO_WEB', $prefijoControlarValidar);
        Configuration::updateValue('ADAMSPAY_LABEL_DEUDAS', Tools::getValue('ADAMSPAY_LABEL_DEUDAS'));
        // Sino está vacia la cadena, actualizamos

        if(Tools::getValue('ADAMSPAY_API_KEY_APP') != ''){
            Configuration::updateValue('ADAMSPAY_API_KEY_APP', Tools::getValue('ADAMSPAY_API_KEY_APP'));
        }

        if(Tools::getValue('ADAMSPAY_API_SECRET_APP') != ''){
            Configuration::updateValue('ADAMSPAY_API_SECRET_APP', Tools::getValue('ADAMSPAY_API_SECRET_APP'));
        }

        Configuration::updateValue('ADAMSPAY_MODO_PRODUCCION', Tools::getValue('ADAMSPAY_MODO_PRODUCCION'));

    }

    /**
     * Crea el Form con Smarty
     */

    protected function renderizarFormulario()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitadamspayModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getFormConfiguracion()));
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {

        return array(
            'ADAMSPAY_PREFIJO_WEB' => Configuration::get('ADAMSPAY_PREFIJO_WEB', null),
            'ADAMSPAY_LABEL_DEUDAS' => Configuration::get('ADAMSPAY_LABEL_DEUDAS', null),
            'ADAMSPAY_API_KEY_APP' => Configuration::get('ADAMSPAY_API_KEY_APP', null),
            'ADAMSPAY_API_SECRET_APP' => Configuration::get('ADAMSPAY_API_SECRET_APP', null),
            'ADAMSPAY_MODO_PRODUCCION' => Configuration::get('ADAMSPAY_MODO_PRODUCCION', null)
        );
    }

    /**
     * Crear el formulario.
     */
    protected function getFormConfiguracion()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Configuración AdamsPay'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Modo Producción'),
                        'name' => 'ADAMSPAY_MODO_PRODUCCION',
                        'is_bool' => true,
                        'desc' => $this->l('Modo Pruebas / Modo Producción'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-text"></i>',
                        'desc' => $this->l('El prefijo para el código de deuda. Máx 10 caracteres. Debe terminar con un \'_\''),
                        'name' => 'ADAMSPAY_PREFIJO_WEB',
                        'label' => $this->l('Prefijo Web')
                    ),
                    array(
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-text"></i>',
                        'desc' => $this->l('Descripción en página externa de Pagos'),
                        'name' => 'ADAMSPAY_LABEL_DEUDAS',
                        'label' => $this->l('Label Deudas')
                    ),
                    array(
                        'type' => 'password',
                        'prefix' => '<i class="icon icon-key"></i>',
                        'desc' => $this->l('AdamsPay.com / Configuración / Aplicaciones / Api Key'),
                        'name' => 'ADAMSPAY_API_KEY_APP',
                        'label' => $this->l('AdamsPay Api Key')
                    ),

                    array(
                        'type' => 'password',
                        'prefix' => '<i class="icon icon-key"></i>',
                        'desc' => $this->l('AdamsPay.com / Configuración / Aplicaciones / Api Secret'),
                        'name' => 'ADAMSPAY_API_SECRET_APP',
                        'label' => $this->l('AdamsPay Api Secret')
                    )

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
            return false;

        $this->smarty->assign('module_dir', $this->_path);
        $this->smarty->assign('mensaje', Tools::getValue('mensaje'));

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        $this->smarty->assign('mensaje', Tools::getValue('mensaje'));
        if ($this->active == false)
            return;

        $order = $params['objOrder'];


        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR'))
            $this->smarty->assign('status', 'ok');

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    /**
     * Return payment options available for PS 1.7+
     *
     * @param array Hook parameters
     *
     * @return array|null
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        $order = $params['objOrder'];

        $descripcion_adamspay = 'Por favor, confirme los términos del servicio y Confirme su pedido,  
            se le redireccionará a la Web Segura de AdamsPay.com donde 
            seleccionará su método de pago preferido para finalizar la compra.';

        $claveCliente = Configuration::get('ADAMSPAY_API_KEY_APP');;
        $option = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $option->setCallToActionText($this->l('AdamsPay - Cobros online para tu eCommerce'))
            ->setAdditionalInformation($this->l($descripcion_adamspay))
            //->setInputs()
            ->setlogo($this->l($this->_path . '/views/img/adams-logo-150px.png'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true));

        return [
            $option
        ];
    }


    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Se usa en el método install() y en update vía post
     * Acorta el parametro PS_SHOP_NAME para poder setear en la configuración del prefijo.
     * Pone en minuscula, recorta hasta 10 caracteres.
     * Ejemplo: "AdamsPay - Online Store" >> "adamspay_o"
     * Luego puede cambair.
     * */

    // @todo [Migs] @done
    // a) El prefijo debe ser configurable y el valor de acotarNombreTienda() utilizado como DEFAULT si no se configuró.
    // b) El prefijo se utilizaría sólo al crear la deuda la 1era vez y luego el ID de la deuda almacenada y asociada
    //    a la orden para evitar fallas en caso que se cambie el prefijo (o nombre del sitio).

    public static function acotarNombreTienda($tieneParametro = null)
    {

        // Dejamos solo letras y numeros.
        $buscar = ['\'', '"',' ', '-', '_', '.', '*', '!', '@', '$', '%', '^', '(', ')', '+', '=', '/', '?', '<', '>', '|'];

        if($tieneParametro){
            // Si tiene parametro, no usamos nombre de tienda, usamos el parametro para limpiar
            $stringTrabajar = str_replace($buscar, '', $tieneParametro);
        }
        else{
            $stringTrabajar = str_replace($buscar, '', Configuration::get('PS_SHOP_NAME'));
        }

        if (strlen($stringTrabajar) > 10) {
            $stringTrabajar = substr($stringTrabajar, 0, 10);
        }

        // strripos buscamos la ultima aparición de '_' Guion. Previamente ya limpiamos que no contenga
        // en el String.
        if(!strripos($stringTrabajar, '_')){
            $stringTrabajar = $stringTrabajar.'_';
        }

        return strtolower( $stringTrabajar );

    }

}
